const { meta, theme } = require("./src/config/config.json");

module.exports = {
  outputDir: "dist",
  transpileDependencies: ["vuetify"],
  chainWebpack: (config) => {
    // svg as component import
    const svgRule = config.module.rule("svg");
    svgRule.uses.clear();
    svgRule
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader")
      .options({ svgo: { plugins: [{ prefixIds: true }] } });
    config.plugin("html").tap((args) => {
      args[0] = {
        ...args[0],
        title: meta.title,
        description: meta.description,
        gtag: meta.gtag || process.env.VUE_APP_GTAG,
        isProd: process.env.NODE_ENV === "production",
        baseUrl: meta.baseUrl,
      };
      // eslint-disable-next-line no-console
      console.log("⚠️", { args: args[0] });
      return args;
    });
  },
  productionSourceMap: false,
  filenameHashing: true,
  pwa: {
    themeColor: theme.primary,
    name: meta.title,
    msTileColor: theme.primary,
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",
    workboxPluginMode: "GenerateSW",
    workboxOptions: {
      clientsClaim: true,
      skipWaiting: true,
      include: [/.svg/, /.png/, /.woff2/, /.css/, /.js/],
      exclude: [
        /\.map$/,
        /manifest\.json$/,
        /\.pdf$/,
        /\.gif$/,
        /precache-manifest/,
        /.tif/,
      ],
      cleanupOutdatedCaches: true,
    },
  },
};
