// https://docs.cypress.io/api/introduction/api.html
import config from "../../../src/config/config.json";

describe("My First Test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", config.name);
  });
});
