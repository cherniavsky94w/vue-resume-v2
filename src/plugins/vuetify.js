import Vue from "vue";
import Vuetify from "vuetify/lib";
import { theme as colors } from "../config/config.json";

Vue.use(Vuetify);

const opts = {};

const theme = {
  themes: {
    light: colors,
    dark: colors,
  },
  dark: localStorage.getItem("dark") === "true",
};

export default new Vuetify({ ...opts, theme });
